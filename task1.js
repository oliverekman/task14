// Task 1 (1) 

// let x = myFunction(4, 6);

// function myFunction(a, b) {

//     return a * b;

// }
// console.log(x)

//-----------------------------------------//

// Task 1 (2)

// let x = myFunction(10, 4, 6);

// function myFunction(a, b, c) {

//     return a + b + c;

// }
// console.log(x)


//---------------------------------------------//

// Task 1 (3)

// let x = myFunction(10);

// function myFunction(a) {

//     return a / 2;

// }
// console.log(x)

//------------------------------------------//

// Task 1 (4)

// let x = myFunction(10, 10, 5);

// function myFunction(a, b, c) {

//     return (a + b) * c;

// }
// console.log(x)

//-----------------------------------------//


// Block Scope
// Block scope is the area within the (if) inside cuy brackets

// function myTest() {
//     if(true) {
//         const animal1 = "dog";  // Block scope
//         let animal2 = "cat";    // Block scope
//     }

//     console.log(animal1);
//     console.log(animal2);
// }

// myTest();

//--------------------------------------------//

// Hoisting
//--------------EXAMPLE1-----------------------//

// function myName(name) {
//      console.log("My name is " + name)
// }

// myName("Oliver");

// //--------------EXAMPLE2---------------//

// myName("Oliver")

// function myName(name) {
//     console.log("My name is " + name)

// }

// I can call the function in code before it is written and it still works //

//---------------------------------------------------------------------------------//

//----- Coercion----------- //

// const add = 1 + "9";


// const add = "My" + " dog" + " is " + 1 + "0" + " years old";


// const add = 0 + false;  // Stays 0


// const add = 0 + true;  // will be 1

// const add = 5 + true; // Will be 6


//----------------------------------------------------------------------//

//---------- Employee-----------//

// function Employee(name, surname, email, number) {
//     this.name = name;
//     this.surname = surname;
//     this.email = email;
//     this.number = number;

// }

// function getFullName(name, surname) {
//      return name + " " + surname;
// }

// const employeeName = getFullName("Oliver", "Ekman");

// function getContactCard(name, surname, number) {
//     return name + " " + surname + " " + " " + "ID " + number;
// }

// const contactCard = getContactCard("Oliver", "Ekman", "0707");




// let oliver = new Employee("Oliver", "Ekman", "eknsf@gmail.com", "0707")


//-------------------------------------------------------------------------//


//-------------- OBJECT --------------------------- //

// const newProject = {
//     projactName: "Full Stack Dev",
//     projectStatus: "In Progress",
//     projectNumber: "940907",
//     projectMembers: "Oliver and Co.",
//     tasks:  {
//         html: "Check",
//         css: "check",
//         bootstrap: "check",
//         aws: "check",
//         javascript: "In progress"
//     } 
// }

//---------------------------------------------------------------------//